import sys
sys.path.append('../')
import regTrees
import numpy as np
from numpy import *
from Ch03 import treePlotter
import matplotlib.pyplot as plt

myDat = regTrees.MyloadDataSet('data.csv')
myDat = np.asarray(myDat)
label = ['AveSpeed', 'AveExtend', 'StdExtend']
# myDat = np.delete(myDat,-1,1)
print(myDat.shape)
# print(myDat[:,2])
# print(myDat)
# myMat = mat(myDat)
# print((myMat[:,2].T.A.tolist())[0])
# # print(myMat)
# print(label)
MyTree = regTrees.createTree(myDat, label=label)
print(MyTree)
# regTrees.createTree(myDat)
# print(MyTree)
# print(type(MyTree))
# myDatTest = regTrees.loadDataSet('ex2test.txt')
# myMat2Test = mat(myDatTest)
#
# PruneTree = regTrees.prune(MyTree, myMat2Test)
# print("Prune Tree")
# print(PruneTree)

# treePlotter.createPlot(MyTree)