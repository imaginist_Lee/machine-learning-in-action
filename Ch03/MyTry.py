import trees
import treePlotter
import copy

mydata, labels = trees.createDataSet()
temp_label = labels.copy()
print(mydata, labels)
# print(trees.chooseBestFeatureToSplit(mydata))
myTree = trees.createTree(mydata, labels)
print(myTree)
# print(myTree)

# myTree = treePlotter.retrieveTree(1)
# print(myTree)
# print(treePlotter.retrieveTree(1))
print(temp_label)
treePlotter.createPlot(myTree)
# print(trees.classify(myTree, temp_label, [1,0]))